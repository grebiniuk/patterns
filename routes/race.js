const path = require('path');
const express = require('express');
const router = express.Router();
const passport = require('passport');

router.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '../race.html'));
});

router.get('/text', passport.authenticate('jwt', {session: false}), function(req, res) {
  res.send(req.game.getText());
});

module.exports = router;
