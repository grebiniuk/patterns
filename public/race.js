window.onload = () => {
  const blockSelector = {
    game: '#game',
    pending: '#pending',
    timer: '#timer',
    playersList: '#players-list',
    text: '#text',
    commentator: '#commentator',
  };

  class InlineElem { // this is facade
    constructor(elem) {
      this.elem = elem;
    }

    show() {
      this.elem.classList.remove('hidden');
    }

    hide() {
      this.elem.className = 'hidden';
    }

    setHTML(html) {
      this.elem.innerHTML = html;
    }

    setText(text) {
      this.elem.innerText = text;
    }

    matches(selector) {
      return this.elem.matches(selector);
    }

  }

  class BlockElem extends InlineElem { // this is facade
    setHTML(html) {
      this.elem.innerHTML = html;
    }
  }

  const createElemWrapper = elem => { // this is factory
    if (elem.matches('span,i,b')) {
      return new InlineElem(elem);
    } else {
      return new BlockElem(elem);
    }
  };

  const getBlock = (() => {//this is proxy
    let blockCache = {};
    return (name) => {
      let elem;
      if (!blockCache[name]) {
        elem = document.querySelector(blockSelector[name]);
        blockCache[name] = createElemWrapper(elem);
      }
      return blockCache[name];
    };
  })();

  const jwt = localStorage.getItem('jwt');
  if (!jwt) {
    location.replace('/login');
  } else {
    const socket = io({
      transportOptions: {
        polling: {
          extraHeaders: {
            'x-token': jwt,
          },
        },
      },
    }).connect('http://localhost:3000');

    document.addEventListener('keypress', function(e) {
      if (currentStage === 'race' && receivedText[index] === e.key) {
        index++;
        let html = '<mark>' +
            receivedText.substring(0, index) + '</mark>' +
            receivedText.substring(index);
        getBlock('text').setHTML(html);
        socket.emit('progress', index);
      }
    });
    let receivedText;
    let index = 0;
    let currentStage;

    socket.on('prepare', () => {
      fetch('http://localhost:3000/race/text', {
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('jwt'),
        },
      }).then(response => response.text()).then(text => receivedText = text);
    });

    socket.on('result', ({players}) => {
      let text = 'the race is finished, look at the results:';
      getBlock('text').setText(text);
      let ol = document.createElement('ol');
      players.sort((p1, p2) => {
        if (p1.finishedAt > p2.finishedAt) {
          return 1;
        } else if (p1.finishedAt < p2.finishedAt) {
          return -1;
        }
        if (p1.process > p2.progress) {
          return 1;
        } else if (p1.progress < p2.progress) {
          return -1;
        }
        return 0;
      });
      ol.innerHTML = players.slice(0, 2).map((player) => {
        return `<li> Player: ${player.login} with the progress of ${player.progress} ${player.finishedAt}</li>`;
      }).join('');
      getBlock('text').elem.appendChild(ol);
    });

    socket.on('comment', receivedText => {
      console.log(receivedText);
      getBlock('commentator').setHTML(receivedText);
    });

    socket.on('tick', data => {
      const {timer, stage, players} = data;
      const showPending = !players || stage === 'wait';
      let announcement;

      currentStage = stage;
      switch (currentStage) {
        case 'wait': {
          announcement = ' until the next race ';
          break;
        }
        case 'race': {
          announcement = ' until the end of the race ';
          break;
        }
        case 'result': {
          announcement = ' until the next round ';
          break;
        }
        default: {
          announcement = 'currentStage is unknown';
        }
      }

      getBlock('timer').setText(String(timer) + ' seconds left ' +
          announcement);
      if (showPending && getBlock('pending').matches('.hidden')) {
        getBlock('pending').show();
        getBlock('game').hide();
      } else if (!showPending && getBlock('pending').matches(':not(.hidden)')) {
        getBlock('pending').hide();
        getBlock('game').show();
        getBlock('text').setText(receivedText);
        index = 0;
      }

      if (players) {
        getBlock('playersList').setHTML(players.map(player => {
          return `<li>${player.login}<span class="progress"><b style="width:${player.progress}%"></b></span></li>`;
        }).join(''));
      }
    });
  }
};
