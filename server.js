const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');

const raceRouter = require('./routes/race');
const loginRouter = require('./routes/login');

const Game = require('./services/game');
const Commentator = require('./services/commentator');
const {stages} = require('./services/game-stages');

require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

const game = new Game();
const commentator = new Commentator(game);

app.use('/race', (req, res, next) => {
  req.game = game;
  next();
});

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.use('/race', raceRouter);
app.use('/login', loginRouter);

io.use((socket, next) => {
  let token = socket.handshake.headers['x-token'];
  let currentUser;
  if (token && (currentUser = jwt.decode(token)).login) {
    socket.currentUser = currentUser;
    return next();
  }
  return next(new Error('authentication error'));
});

io.on('connection', (socket) => {
  const user = socket.currentUser;

  game.addUser(user.login);
  socket.join('pending');
  if (game.getStage() === stages.wait) {
    socket.emit('prepare');
  }

  socket.on('disconnect', () => {
    game.removeUser(user.login);
  });

  socket.on('progress', (index) => {
    game.setRaceProgress(user.login, index);
  });

  game.on('start', () => {
    console.log('start');
    if (game.hasPlayingUser(user.login)) {
      socket.leave('pending');
      socket.join('game');
    }
  });

  game.on('stop', () => {
    let players = game.getPlayers();
    socket.to('game').emit('result', {players});
  });

  game.on('wait', () => {
    if (game.hasPendingUser(user.login)) {
      socket.leave('game');
      socket.join('pending');
    }
    socket.emit('prepare');
  });
});

game.on('tick', data => {
  const {timer, stage} = data;
  io.to('game').emit('tick', data);
  io.to('pending').emit('tick', {timer, stage});
});

commentator.on('comment', ({room = 'game', msg}) => {
  io.to(room).emit('comment', msg)
});

game.turnOn();