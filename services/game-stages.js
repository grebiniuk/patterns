const gameTime = 4 * 60 /10;
const waitTime = 20 / 5;
const resultTime = 60 /10;

const stages = {
  wait: 'wait',
  race: 'race',
  result: 'result'
};

module.exports = {
  stages,
  gameTime,
  waitTime,
  resultTime
};