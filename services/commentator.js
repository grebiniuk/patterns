const EventEmitter = require('events');
const {stages, gameTime} = require('./game-stages');
const jokes = require('../db/jokes.json');

class Commentator extends EventEmitter{
  constructor(game) {
    super();
    this.game = game;
    this.game.on('tick', this.onTick.bind(this));
    this.commentExp = 0;
  }

  onTick({stage, timer}) {
    if (++this.commentExp === 5) {
      let room = stage === stages.wait ? 'pending': 'game';
      let joke = jokes[Math.floor(Math.random() * jokes.length)];
      this.comment({room: room, msg: joke});
    }

    if (stage === stages.wait && timer === 1) {
      this.comment({room: 'pending', msg: 'Ready'});
    } else if (stage === stages.wait && timer === 0) {
      this.comment({room: 'pending', msg: 'Steady'});
    } else if (stage === stages.race && timer === gameTime - 1) {
      this.comment({room: 'game', msg: 'GO!!!!!'});
    } else if (stage === stages.race && timer === 3) {
      this.comment({msg: 'three minutes left'});
    }
  }

  comment(comment) {
    this.emit('comment', comment);
    this.commentExp = 0;
  }

}

module.exports = Commentator;